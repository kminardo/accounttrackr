﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AccountTrackr.Controllers.View
{
    public class InventoryController : Controller
    {
        //
        // GET: /Inventory/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult NewCategory()
        {
            return View();
        }

        public ActionResult NewItem()
        {
            return View();
        }
	}
}