﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Runtime.Serialization;
using AccountTrackr.Models.Inventory;
using AccountTrackr.Models.Inventory.DTO;

namespace AccountTrackr.Controllers.API
{
    public class CategoryController : ApiController
    {
        static readonly CategoryDAL _db = new CategoryDAL();

        // GET api/Category
        public dynamic GetCategories()
        {
            return _db.GetAll();
        }

        public dynamic PostCategory(CategoryDTO newCategory)
        {
            return _db.Add(newCategory);
        }

        public bool DeleteCategory(int id)
        {
            return _db.DeleteCategory(id);
        }
    }
}