﻿using AccountTrackr.Models.Inventory;
using AccountTrackr.Models.Inventory.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AccountTrackr.Controllers.API
{
    public class attributeParamObj
    {
        public int CategoryId { get; set; }
        public int InvetoryItemId { get; set; }
    }

    public class AttributeController : ApiController
    {
        AttributeDAL _db = new AttributeDAL();

        public List<AttributeDTO> GetItemAttributesWithValuesByItemId(int id)
        {
            return _db.GetItemAttributesWithValuesByItemId(id);
        }

        public List<AttributeDTO> GetItemAttributesByCategoryId(int id)
        {
            return _db.GetItemAttributesByCategoryId(id);
        }
    }
}
