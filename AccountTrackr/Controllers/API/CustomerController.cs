﻿using AccountTrackr.Models;
using AccountTrackr.Models.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AccountTrackr.Controllers.Api
{
    public class CustomerController : ApiController
    {
        static readonly CustomerDAL _db = new CustomerDAL();

        public List<Customer> Get()
        {
            return _db.GetAll();
        }

        public Customer Post(Customer newAccount)
        {

            //TODO: Remove quick hack to put lots of users in the database

            //_db.Add(newAccount);
            //string staticname = newAccount.LastName;

            //for (int i = 0; i < 50; i++)
            //{
            //    newAccount.LastName = staticname + " " + i.ToString();
            //    _db.Add(newAccount);
            //}

            //return newAccount;

            return _db.Add(newAccount);
        }

        public Customer Put(Customer newAccount)
        {
            return _db.Update(newAccount);
        }

        public bool Delete(int removeAccountId)
        {
            return _db.Delete(removeAccountId); ;
        }
    }
}
