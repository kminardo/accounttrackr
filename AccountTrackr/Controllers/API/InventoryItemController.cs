﻿using AccountTrackr.Models.Inventory;
using AccountTrackr.Models.Inventory.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AccountTrackr.Controllers.API
{
    public class InventoryItemController : ApiController
    {
        static readonly InventoryItemDAL _db = new InventoryItemDAL();

        public List<InventoryItemDTO> GetInventoryItemByCatId(int id)
        {
            return _db.GetInventoryItemByCategoryId(id);
        }

        public InventoryItemDTO PostNewInventoryItem(InventoryItemDTO itemToAdd)
        {
            return _db.AddNewInventoryItem(itemToAdd);
        }

        public bool DeleteInventoryItem(int id)
        {
            return _db.DeleteInventoryItem(id);
        }
    }
}
