﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AccountTrackr.Support
{
    //Example:
    //Crypto _crypto = new Crypto();
    //var newSalt = _crypto.GenerateSalt();
    //var newSaltString = _crypto.HashToString(newSalt);
    //var newHashString = _crypto.HashToString(_crypto.GenerateHash(userPass, newSalt));

    //Authenticate:
    //var dbSalt = _crypto.StringToHash(dbSaltString); // dbSaltString from Database
    //var hashToCheck = _crypto.GenerateHash(userPass, dbSalt); //Generate Hash from User Password
    //var check = Crypto.CompareByteArrays(_crypto.StringToHash(dbHashString), hashToCheck) //dbHashString from Database vs hashToCheck

    public class Crypto
    {
        const int ITERATIONS = 1000;

        public byte[] GenerateSalt()
        {
            var rng = new System.Security.Cryptography.RNGCryptoServiceProvider();
            byte[] salt = new byte[8];
            rng.GetBytes(salt);
            return salt;
        }

        public byte[] GenerateHash(string pwd, byte[] salt)
        {
            var pbkdf2 = new System.Security.Cryptography.Rfc2898DeriveBytes(pwd, salt, ITERATIONS);
            var hash = pbkdf2.GetBytes(16);
            return hash;
        }

        public string HashToString(byte[] hash)
        {
            var hashAsString = Convert.ToBase64String(hash);
            return hashAsString;
        }

        public byte[] StringToHash(string hashAsString)
        {
            var hashAsBytes = Convert.FromBase64String(hashAsString);
            return hashAsBytes;
        }

        public static bool CompareByteArrays(byte[] array1, byte[] array2)
        {
            if (array1.Length != array2.Length)
            {
                return false;
            }

            for (int i = 0; i < array1.Length; i++)
            {
                if (array1[i] != array2[i])
                {
                    return false;
                }
            }

            return true;
        }
    }
}