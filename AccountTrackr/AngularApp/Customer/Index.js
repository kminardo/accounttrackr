﻿/// <reference path="../../Scripts/_references.js" />

angular.module('main')
    .controller('Customer.IndexController', ['$scope', '$http', function ($scope, $http) {
        //Variables
        $scope.customerRecord = {}
        $scope.loading = true;

        //Functions
        $scope.GetCustomers = function (callback) {
            $scope.loading = true;
            $http.get('/api/customer').success(function (listOfCustomers) {
                $scope.currentCustomers = listOfCustomers;
                $scope.loading = false
                typeof callback === 'function' && callback();
            });
        }

        $scope.Add = function (newCustomer) {
            $http.post('/api/customer', newCustomer).success(function (newCustomerWithId) {
                $scope.GetCustomers(function () {
                    //TODO: Make this a user setting, should the new user persist or clear out?
                    //customerIndex = findInCustomerList(newCustomerWithId.id);
                    //$scope.newCustomer = $scope.currentCustomers[customerIndex];
                    $scope.customerRecord = {};
                    toastr.success('Customer Saved!');
                });
            });
        }

        $scope.Update = function (updateCustomer) {
            $http.put('/api/customer', updateCustomer).success(function (updatedCustomer) {
                $scope.GetCustomers(function () {
                    toastr.success('Customer Updated!');
                });
            });
        }

        $scope.Delete = function (removeCustomer) {
            if (confirm('Are you sure you want to remove ' + removeCustomer.firstName + ' ' + removeCustomer.lastName + '?')) {
                $http.delete('/api/customer', { params: { 'removeAccountId': removeCustomer.id } }).success(function () {
                    $scope.GetCustomers(function () {
                        $scope.customerRecord = {};
                        toastr.success('Customer Removed!');
                    });
                });
            }
        }

        $scope.Reset = function () {
            $scope.customerRecord = {};
        }

        function findInCustomerList(ID) {
            var locationInIndex;
            for (var i = 0; i < $scope.currentCustomers.length; i++) {
                var customer = $scope.currentCustomers[i];
                if (customer.id === ID) {
                    locationInIndex = i;
                } else {
                    locationInIndex - 1
                }
            }
            return locationInIndex
        };

        //OnLoad
        $scope.GetCustomers(function () {
            //Setup pages after we have records
            $scope.numberOfPages = function () {
                return Math.ceil($scope.currentCustomers.length / $scope.pageSize);
            };
        });

        //Paging
        $scope.currentPage = 0;
        $scope.pageSize = 10;
        $scope.setCurrentPage = function (currentPage) {
            $scope.currentPage = currentPage;
        }

        $scope.getNumberAsArray = function (num) {
            return new Array(num);
        };
    }]);