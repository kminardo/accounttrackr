﻿angular.module('main')
    .factory('attributeFactory', ['$http', function ($http) {

        var urlBase = '/api/attribute';
        var dataFactory = {};

        dataFactory.GetItemAttributesWithValuesByItemId = function (inventoryItemId) {
            return $http.get(urlBase + '/GetItemAttributesWithValuesByItemId/' + inventoryItemId);
        };

        dataFactory.GetItemAttributesByCategoryId = function (categoryId) {
            return $http.get(urlBase + '/GetItemAttributesByCategoryId/' + categoryId);
        };

        return dataFactory;
    }]);