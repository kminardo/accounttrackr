﻿angular.module('main')
    .factory('inventoryItemFactory', ['$http', function ($http) {

        var urlBase = '/api/inventoryItem';
        var dataFactory = {};

        dataFactory.getInventoryItemsByCatId = function (id) {
            return $http.get(urlBase + '/' + id);
        };

        dataFactory.addItem = function (item) {
            return $http.post(urlBase, item);
        };

        dataFactory.deleteItem = function (itemId) {
            return $http.delete(urlBase + '/' + itemId);
        };

        return dataFactory;
    }]);