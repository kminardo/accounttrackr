﻿angular.module('main')
    .factory('categoryFactory', ['$http', function ($http) {

        var urlBase = '/api/category';
        var dataFactory = {};

        dataFactory.getCategories = function () {
            return $http.get(urlBase);
        };

        dataFactory.getCategory = function (id) {
            return $http.get(urlBase + '/' + id);
        };

        dataFactory.addCategory = function (cat) {
            return $http.post(urlBase, cat);
        };

        //dataFactory.updateCategory = function (cust) {
        //    return $http.put(urlBase + '/' + cust.ID, cust)
        //};

        dataFactory.deleteCategory = function (id) {
            return $http.delete(urlBase + '/' + id);
        };

        return dataFactory;
    }]);