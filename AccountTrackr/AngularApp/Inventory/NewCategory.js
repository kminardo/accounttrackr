﻿/// <reference path="../../Scripts/_references.js" />

angular.module('main')
    .controller('Inventory.NewCategoryController', ['$scope', '$http','$location', 'categoryFactory', function ($scope, $http, $location, categoryFactory) {

        $scope.title = 'New Category';

        //Build out an empty record to populate
        $scope.categoryRecord = {
            name: '',
            attributes: [{ name: '' }]
        }

        $scope.AddNewAttribute = function () {
            $scope.categoryRecord.attributes.push({ name: '' });
        }

        $scope.SaveNewCategory = function (catToSave) {
            categoryFactory.addCategory(catToSave).success(function () {
                $location.path('/inventory');
            });
        };
    }]);