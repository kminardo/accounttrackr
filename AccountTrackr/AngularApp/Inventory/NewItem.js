﻿/// <reference path="../../Scripts/_references.js" />

angular.module('main')
    .controller('Inventory.NewItemController', ['$scope', '$http', '$location', '$routeParams', 'attributeFactory', 'inventoryItemFactory', function ($scope, $http, $location, $routeParams, attributeFactory, inventoryItemFactory) {

        $scope.title = 'New Item';

        //$scope.AddNewAttribute = function () {
        //    $scope.categoryRecord.attributes.push({ name: '' });
        //}

        $scope.SaveNewItem = function (itemToSave) {
            itemToSave.category.id = $routeParams.id;
            inventoryItemFactory.addItem(itemToSave).success(function () {
                $location.path('/inventory');
            });
        };

        //On load
        attributeFactory.GetItemAttributesByCategoryId($routeParams.id)
            .success(function (data, status, headers, config) {
                //Build out an empty record to populate

                $scope.inventoryItem = {
                    name: '',
                    stockCount: 0
                };

                $scope.inventoryItem.category = {
                    id: $routeParams.id
                };

                //We need attribute IDs for the controller on the server side, so we'll hang onto those.
                $scope.inventoryItem.attributeValues = [];
                for (var i = 0; i < data.length; i++) {
                    var newAttrVal = {
                        value: '',
                        attribute: {
                            id: data[i].id,
                            name: data[i].name
                        }
                    };
                    $scope.inventoryItem.attributeValues.push(newAttrVal);
                }


            });

    }]);