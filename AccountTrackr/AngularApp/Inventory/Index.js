﻿/// <reference path="../../Scripts/_references.js" />

angular.module('main')
    .controller('Inventory.IndexController', ['$scope', '$http', 'categoryFactory', 'inventoryItemFactory', 'attributeFactory', function ($scope, $http, categoryFactory, inventoryItemFactory, attributeFactory) {
        $scope.title = "inventory page";

        $scope.currentCategories = [];
        $scope.itemsInCategory = [];
        $scope.selectedItemAttributes = [];

        $scope.selectedCategoryId = null;
        $scope.selectedItemId = null;

        $scope.loadInventoryItems = function (id, bypassIdCheck) {
            if (id != $scope.selectedCategoryId || bypassIdCheck) {
                $scope.selectedCategoryId = id;
                $scope.selectedItemAttributes = [];

                inventoryItemFactory.getInventoryItemsByCatId($scope.selectedCategoryId)
                    .success(function (data, status, headers, config) {
                        $scope.itemsInCategory = data;

                        //if we have data let's get the attributes for the first item
                        if (data.length > 0) {
                            $scope.loadItemAttributesWithValues(data[0].id, true);
                        };

                    });
            }
        };

        $scope.loadItemAttributesWithValues = function (id, bypassIdCheck) {
            if (id != $scope.selectedItemId || bypassIdCheck) {
                $scope.selectedItemId = id;
                attributeFactory.GetItemAttributesWithValuesByItemId($scope.selectedItemId)
                    .success(function (data, status, headers, config) {
                        $scope.selectedItemAttributes = data;
                    });
            }
        };

        $scope.deleteCategory = function (categoryToDel) {
            if (window.confirm("Are you sure you want to remove " + categoryToDel.name + "? \n\n**THIS WILL DELETE ALL ITEMS AND ATTRIBUTES IN THIS CATEGORY!")) {
                categoryFactory.deleteCategory(categoryToDel.id)
                    .success(function (data) {
                        for (var i = $scope.currentCategories.length; i--;) {
                            if ($scope.currentCategories[i].id === categoryToDel.id) {
                                $scope.currentCategories.splice(i, 1);

                                //Reset required value if we just deleted the selected object
                                if ($scope.selectedCategoryId == categoryToDel.id) {
                                    $scope.selectedCategoryId = null;
                                    $scope.selectedItemId = null;
                                    $scope.itemsInCategory = [];
                                }
                            }
                        }
                    })
                    .error(function (data) {
                        console.log('Error: ' + data);
                    });
            }
        };

        $scope.deleteItem = function (item) {
            if (window.confirm("Are you sure you want to remove " + item.name + "?")) {
                inventoryItemFactory.deleteItem(item.id)
                    .success(function (data) {
                        for (var i = $scope.itemsInCategory.length; i--;) {

                            //Reset required value if we just deleted the selected object
                            if ($scope.itemsInCategory[i].id === item.id) {
                                $scope.itemsInCategory.splice(i, 1);
                                if ($scope.selectedItemId == item.id) {
                                    $scope.selectedItemId = null;
                                    $scope.selectedItemAttributes = [];
                                }
                            }
                        }
                    })
                    .error(function (data) {
                        console.log('Error: ' + data);
                    });
            }
        };

        //On load
        categoryFactory.getCategories()
            .success(function (data, status, headers, config) {
                $scope.currentCategories = data;
            });
    }]);