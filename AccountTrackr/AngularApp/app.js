﻿//Main configuration file. Sets up AngularJS module and routes and any other config objects

var appRoot = angular.module('main', ['ngRoute']);     //Define the main module

appRoot
    .config(['$routeProvider', function ($routeProvider) {
        //Setup routes to load partial templates from server. TemplateUrl is the location for the server view (Razor .cshtml view)
        $routeProvider
            .when('/home', {
                templateUrl: '/Landing/Index', controller: 'Landing.IndexController', title: 'AccountTrackr',
            })
            .when('/customer', {
                templateUrl: '/Customer/Index', controller: 'Customer.IndexController', title: 'Customers',
            })
            .when('/inventory', {
                templateUrl: '/Inventory/Index', controller: 'Inventory.IndexController', title: 'Inventory',
            })
            .when('/inventory/newcategory', {
                templateUrl: '/Inventory/NewCategory', controller: 'Inventory.NewCategoryController', title: 'New Category',
            })
            .when('/inventory/newitem/:id', {
                templateUrl: '/Inventory/NewItem', controller: 'Inventory.NewItemController', title: 'New Item',
            })
            .otherwise({ redirectTo: '/home' });
    }])

    .controller('RootController', ['$scope', '$route', '$routeParams', '$location', function ($scope, $route, $routeParams, $location) {
        $scope.$on('$routeChangeSuccess', function (e, current, previous) {
            $scope.activeViewPath = $location.path();
            $scope.title = current.$$route.title;
        });
    }]);

appRoot.filter('startFrom', function () {
    return function (input, start) {
        if (input === undefined || input === null || input.length === 0) return [];
        return input.slice(start);
    };
});