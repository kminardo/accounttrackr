﻿using System.Web;
using System.Web.Optimization;

namespace AccountTrackr
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                        "~/Scripts/angular.js",
                        "~/Scripts/angular-ng-grid.js",
                        "~/Scripts/angular-route.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                        "~/Scripts/bootstrap.js"));

            bundles.Add(new ScriptBundle("~/bundles/toastr").Include(
                        "~/Scripts/toastr.js"));

            bundles.Add(new StyleBundle("~/Content/bootstrap").Include(
                        "~/Content/bootstrap.css",
                        "~/Content/bootstrap-responsive.css",
                        "~/Content/bootstrap-custom.css"
                        ));

            bundles.Add(new StyleBundle("~/Content/toastr").Include(
                        "~/Content/toastr.css"
                        ));

            bundles.Add(new StyleBundle("~/Content/fontawesome").Include(
                        "~/Content/FontAwesome/css/font-awesome.min.css"
                        ));

            bundles.Add(new StyleBundle("~/Content/site").Include(
                        "~/Content/Site.css",
                        "~/Content/fonts.css"
                        ));

            #region AngularApp
            bundles.Add(new ScriptBundle("~/bundles/angularapp").Include(
                        "~/AngularApp/*.js",
                        "~/AngularApp/Services/*.js",
                        "~/AngularApp/Landing/*.js",
                        "~/AngularApp/Customer/*.js",
                        "~/AngularApp/Inventory/*.js"));
            #endregion
        }
    }
}
