﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AccountTrackr.Startup))]
namespace AccountTrackr
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
