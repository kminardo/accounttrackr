﻿using AccountTrackr.Models.Inventory.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AccountTrackr.Models.Inventory
{
    public class CategoryDAL
    {
        AccountTrackrContainer db = new AccountTrackrContainer();

        public List<CategoryDTO> GetAll()
        {
            //Need to build out our own DTOs populated how we want em.
            List<Category> queryCat = db.Categories.ToList();

            List<CategoryDTO> returnCategories = new List<CategoryDTO>();

            foreach (var category in queryCat)
            {
                CategoryDTO newCat = new CategoryDTO()
                {
                    Id = category.Id,
                    Name = category.Name,
                };
                returnCategories.Add(newCat);
            }
            return returnCategories;
        }

        public CategoryDTO Add(CategoryDTO newCategory)
        {
            //Convert DTO back to Entity Model
            Category catToSave = new Category()
            {
                Name = newCategory.Name
            };

            foreach (var attribute in newCategory.Attributes)
            {
                Attribute attrToSave = new Attribute()
                {
                    Category = catToSave,
                    Name = attribute.Name
                };

                catToSave.Attributes.Add(attrToSave);
            }

            db.Categories.Add(catToSave);
            db.SaveChanges();

            //Populate our DTOs with the ids before returning
            newCategory.Id = catToSave.Id;
            foreach (var savedAttr in catToSave.Attributes)
            {
                newCategory.Attributes.Where(x => x.Name == savedAttr.Name).First().Id = savedAttr.Id;
            }

            return newCategory;
        }

        public Customer Update(Customer updatedCategory)
        {
            var categoryToUpdate = db.Categories.Where(c => c.Id == updatedCategory.Id).FirstOrDefault();
            db.Entry(categoryToUpdate).CurrentValues.SetValues(updatedCategory);
            db.SaveChanges();

            return updatedCategory;
        }

        public bool DeleteCategory(int id)
        {
            //THIS DELETES ALL ITEMS IN THE CATEGORY!
            Category categoryToDel = db.Categories.Where(c => c.Id == id).FirstOrDefault();

            //clean up attributes and items
            List<Attribute> attrsToDel = db.Attributes.Where(x => x.Category.Id == categoryToDel.Id).ToList();
            db.Attributes.RemoveRange(attrsToDel);

            List<InventoryItem> itemsToDel = db.InventoryItems.Where(x => x.Category.Id == categoryToDel.Id).ToList();

            foreach (var item in itemsToDel)
            {
                List<AttributeValue> valsToDel = db.AttributeValues.Where(x => x.InventoryItem.Id == item.Id).ToList();
                db.AttributeValues.RemoveRange(valsToDel);
                db.InventoryItems.Remove(item);
            }

            db.Categories.Remove(categoryToDel);
            db.SaveChanges();

            //TODO: Investigate correct way to verify record is removed, until then always return true
            return true;
        }
    }
}