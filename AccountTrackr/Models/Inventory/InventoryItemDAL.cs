﻿using AccountTrackr.Models.Inventory.DTO;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace AccountTrackr.Models.Inventory
{
    public class InventoryItemDAL
    {
        AccountTrackrContainer db = new AccountTrackrContainer();

        public List<InventoryItemDTO> GetInventoryItemByCategoryId(int id)
        {
            List<InventoryItem> invItemQuery = db.InventoryItems.Where(x => x.Category.Id == id).ToList();
            List<InventoryItemDTO> returnInvItems = new List<InventoryItemDTO>();

            foreach (var invItem in invItemQuery)
            {
                InventoryItemDTO newInvItem = new InventoryItemDTO()
                {
                    Id = invItem.Id,
                    Name = invItem.Name,
                    DateCreated = invItem.DateCreated,
                    StockCount = invItem.StockCount,
                    Photos = invItem.Photos
                };

                returnInvItems.Add(newInvItem);
            }

            return returnInvItems;
        }

        internal InventoryItemDTO AddNewInventoryItem(InventoryItemDTO newItem)
        {
            //Get the category to save
            Category catFromDb = db.Categories.Where(x => x.Id == newItem.Category.Id).FirstOrDefault();

            InventoryItem itemToSave = new InventoryItem()
            {
                Name = newItem.Name,
                Category = catFromDb,
                StockCount = newItem.StockCount,
                DateCreated = DateTime.Now
            };

            foreach (var attributeValue in newItem.AttributeValues)
            {
                Attribute attrFromDb = db.Attributes.Where(x => x.Id == attributeValue.Attribute.Id).FirstOrDefault();
                itemToSave.AttributeValues.Add(new AttributeValue()
                {
                    Attribute = attrFromDb,
                    InventoryItem = itemToSave,
                    Value = attributeValue.Value
                });
            }

            db.InventoryItems.Add(itemToSave);
            db.SaveChanges();

            return newItem;
        }

        public bool DeleteInventoryItem(int id)
        {
            //Get the item to delete
            InventoryItem itemToDel = db.InventoryItems.Where(x => x.Id == id).FirstOrDefault();

            //Cleanup the attribute values table
            List<AttributeValue> valsToDel = db.AttributeValues.Where(x => x.InventoryItem.Id == itemToDel.Id).ToList();
            db.AttributeValues.RemoveRange(valsToDel);

            db.InventoryItems.Remove(itemToDel);

            db.SaveChanges();

            return true;
        }
    }
}