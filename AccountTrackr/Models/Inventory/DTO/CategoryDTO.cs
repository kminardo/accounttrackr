﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AccountTrackr.Models.Inventory.DTO
{
    public class CategoryDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual List<InventoryItemDTO> InventoryItems { get; set; }
        public virtual List<AttributeDTO> Attributes { get; set; }
    }
}