﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AccountTrackr.Models.Inventory.DTO
{
    public class InventoryItemDTO
    {
        public int Id { get; set; }
        public System.DateTime DateCreated { get; set; }
        public string Name { get; set; }
        public string Photos { get; set; }
        public int StockCount { get; set; }

        //public virtual Customer Customer { get; set; }
        //public virtual ICollection<Tag> Tags { get; set; }
        public virtual CategoryDTO Category { get; set; }
        public virtual List<AttributeValueDTO> AttributeValues { get; set; }
    }
}