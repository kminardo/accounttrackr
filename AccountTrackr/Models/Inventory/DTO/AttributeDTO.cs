﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AccountTrackr.Models.Inventory.DTO
{
    public class AttributeDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual CategoryDTO Category { get; set; }
        public virtual List<AttributeValueDTO> AttributeValues { get; set; }
    }
}