﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AccountTrackr.Models.Inventory.DTO
{
    public class AttributeValueDTO
    {
        public int Id { get; set; }
        public string Value { get; set; }

        public virtual AttributeDTO Attribute { get; set; }
        public virtual InventoryItemDTO InventoryItem { get; set; }
    }
}