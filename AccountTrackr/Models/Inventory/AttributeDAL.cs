﻿using AccountTrackr.Models.Inventory.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AccountTrackr.Models.Inventory
{
    public class AttributeDAL
    {
        AccountTrackrContainer db = new AccountTrackrContainer();

        public List<AttributeDTO> GetItemAttributesWithValuesByItemId(int inventoryItemId)
        {
            //Look up the inventory id by Id so we can get the category... not sure if I did something wrong but WebAPI won't allow me to pass two parameters in a get request. Highly annoying.
            InventoryItem invItem = db.InventoryItems.Where(x => x.Id == inventoryItemId).FirstOrDefault();

            int categoryId = invItem.Category.Id;

            List<Attribute> queryAttr = db.Attributes.Where(x => x.Category.Id == categoryId).ToList();
            List<AttributeDTO> returnAttributes = new List<AttributeDTO>();

            foreach (var attribute in queryAttr)
            {
                AttributeDTO newAttr = new AttributeDTO()
                {
                    Id = attribute.Id,
                    Name = attribute.Name,
                };

                List<AttributeValue> queryAttrVal = db.AttributeValues.Where(x => x.Attribute.Id == attribute.Id && x.InventoryItem.Id == inventoryItemId).ToList();
                List<AttributeValueDTO> returnAttrVals = new List<AttributeValueDTO>();

                foreach (var attributeValue in queryAttrVal)
                {
                    AttributeValueDTO newAttrVal = new AttributeValueDTO()
                    {
                        Id = attributeValue.Id,
                        Value = attributeValue.Value,
                    };

                    returnAttrVals.Add(newAttrVal);
                }
                newAttr.AttributeValues = returnAttrVals;
                returnAttributes.Add(newAttr);
            }

            return returnAttributes;
        }

        public List<AttributeDTO> GetItemAttributesByCategoryId(int categoryId)
        {
            List<Attribute> queryAttr = db.Attributes.Where(x => x.Category.Id == categoryId).ToList();
            List<AttributeDTO> returnAttributes = new List<AttributeDTO>();

            foreach (var attribute in queryAttr)
            {
                AttributeDTO newAttr = new AttributeDTO()
                {
                    Id = attribute.Id,
                    Name = attribute.Name,
                    AttributeValues = new List<AttributeValueDTO>(),
                    Category = new CategoryDTO()
                    {
                        Id = categoryId,
                    }
                };

                newAttr.AttributeValues.Add(new AttributeValueDTO()
                {
                    Value = "",
                    Attribute = new AttributeDTO()
                    {
                        Id = newAttr.Id
                    }
                });

                returnAttributes.Add(newAttr);
            }

            return returnAttributes;
        }
    }
}