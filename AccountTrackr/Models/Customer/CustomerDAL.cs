﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

using AccountTrackr.Models;


namespace AccountTrackr.Models
{
    public class CustomerDAL
    {
        AccountTrackrContainer db = new AccountTrackrContainer();

        public List<Customer> GetAll()
        {
            List<Customer> query = db.Customers.ToList();

            return query;
        }

        public Customer Add(Customer newAccount)
        {
            //set required parameters
            newAccount.DateCreated = DateTime.Now;

            db.Customers.Add(newAccount);
            db.SaveChanges();

            return newAccount;
        }

        public Customer Update(Customer updatedAccount)
        {
            var accountToUpdate = db.Customers.Where(c => c.Id == updatedAccount.Id).FirstOrDefault();
            db.Entry(accountToUpdate).CurrentValues.SetValues(updatedAccount);
            db.SaveChanges();

            return updatedAccount;
        }

        public bool Delete(int removeAccountId)
        {
            var accountToDel = db.Customers.Where(c => c.Id == removeAccountId).FirstOrDefault();
            db.Customers.Remove(accountToDel);
            db.SaveChanges();

            //TODO: Investigate correct way to verify record is removed, until then always return true
            return true;
        }
    }
}