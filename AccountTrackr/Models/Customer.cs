//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AccountTrackr.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Customer
    {
        public Customer()
        {
            this.CurrentInventoryItems = new HashSet<InventoryItem>();
        }
    
        public int Id { get; set; }
        public System.DateTime DateCreated { get; set; }
        public Nullable<decimal> Balance { get; set; }
        public Nullable<decimal> Credit { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Phone2 { get; set; }
        public string Email { get; set; }
        public string Email2 { get; set; }
        public string Address { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public string Comment { get; set; }
        public string PassHash { get; set; }
        public string PassSalt { get; set; }
    
        public virtual ICollection<InventoryItem> CurrentInventoryItems { get; set; }
    }
}
