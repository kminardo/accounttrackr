
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 02/10/2014 19:14:41
-- Generated from EDMX file: C:\Users\Ken\Documents\GitHub\accounttrackr\AccountTrackr\Models\AccountTrackr.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [accounttrackr];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_CustomerInventoryItem]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[InventoryItems] DROP CONSTRAINT [FK_CustomerInventoryItem];
GO
IF OBJECT_ID(N'[dbo].[FK_InventoryItemTag_InventoryItem]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[InventoryItemTag] DROP CONSTRAINT [FK_InventoryItemTag_InventoryItem];
GO
IF OBJECT_ID(N'[dbo].[FK_InventoryItemTag_Tag]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[InventoryItemTag] DROP CONSTRAINT [FK_InventoryItemTag_Tag];
GO
IF OBJECT_ID(N'[dbo].[FK_InventoryItemCategory]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[InventoryItems] DROP CONSTRAINT [FK_InventoryItemCategory];
GO
IF OBJECT_ID(N'[dbo].[FK_CategoryAttribute]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Attributes] DROP CONSTRAINT [FK_CategoryAttribute];
GO
IF OBJECT_ID(N'[dbo].[FK_AttributeAttributeValue]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AttributeValues] DROP CONSTRAINT [FK_AttributeAttributeValue];
GO
IF OBJECT_ID(N'[dbo].[FK_InventoryItemAttributeValue]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AttributeValues] DROP CONSTRAINT [FK_InventoryItemAttributeValue];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Customers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Customers];
GO
IF OBJECT_ID(N'[dbo].[InventoryItems]', 'U') IS NOT NULL
    DROP TABLE [dbo].[InventoryItems];
GO
IF OBJECT_ID(N'[dbo].[Tags]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Tags];
GO
IF OBJECT_ID(N'[dbo].[Categories]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Categories];
GO
IF OBJECT_ID(N'[dbo].[Attributes]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Attributes];
GO
IF OBJECT_ID(N'[dbo].[AttributeValues]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AttributeValues];
GO
IF OBJECT_ID(N'[dbo].[InventoryItemTag]', 'U') IS NOT NULL
    DROP TABLE [dbo].[InventoryItemTag];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Customers'
CREATE TABLE [dbo].[Customers] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [DateCreated] datetime  NOT NULL,
    [Balance] decimal(18,0)  NULL,
    [Credit] decimal(18,0)  NULL,
    [FirstName] nvarchar(max)  NOT NULL,
    [LastName] nvarchar(max)  NOT NULL,
    [Phone] nvarchar(max)  NULL,
    [Phone2] nvarchar(max)  NULL,
    [Email] nvarchar(max)  NULL,
    [Email2] nvarchar(max)  NULL,
    [Address] nvarchar(max)  NULL,
    [State] nvarchar(max)  NULL,
    [Country] nvarchar(max)  NULL,
    [ZipCode] nvarchar(max)  NULL,
    [Comment] nvarchar(max)  NULL,
    [PassHash] nvarchar(max)  NULL,
    [PassSalt] nvarchar(max)  NULL
);
GO

-- Creating table 'InventoryItems'
CREATE TABLE [dbo].[InventoryItems] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [DateCreated] datetime  NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Photos] nvarchar(max)  NULL,
    [StockCount] int  NOT NULL,
    [Customer_Id] int  NULL,
    [Category_Id] int  NULL
);
GO

-- Creating table 'Tags'
CREATE TABLE [dbo].[Tags] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Text] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Categories'
CREATE TABLE [dbo].[Categories] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Attributes'
CREATE TABLE [dbo].[Attributes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Category_Id] int  NOT NULL
);
GO

-- Creating table 'AttributeValues'
CREATE TABLE [dbo].[AttributeValues] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Value] nvarchar(max)  NOT NULL,
    [Attribute_Id] int  NULL,
    [InventoryItem_Id] int  NOT NULL
);
GO

-- Creating table 'InventoryItemTag'
CREATE TABLE [dbo].[InventoryItemTag] (
    [InventoryItem_Id] int  NOT NULL,
    [Tags_Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Customers'
ALTER TABLE [dbo].[Customers]
ADD CONSTRAINT [PK_Customers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'InventoryItems'
ALTER TABLE [dbo].[InventoryItems]
ADD CONSTRAINT [PK_InventoryItems]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Tags'
ALTER TABLE [dbo].[Tags]
ADD CONSTRAINT [PK_Tags]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Categories'
ALTER TABLE [dbo].[Categories]
ADD CONSTRAINT [PK_Categories]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Attributes'
ALTER TABLE [dbo].[Attributes]
ADD CONSTRAINT [PK_Attributes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AttributeValues'
ALTER TABLE [dbo].[AttributeValues]
ADD CONSTRAINT [PK_AttributeValues]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [InventoryItem_Id], [Tags_Id] in table 'InventoryItemTag'
ALTER TABLE [dbo].[InventoryItemTag]
ADD CONSTRAINT [PK_InventoryItemTag]
    PRIMARY KEY CLUSTERED ([InventoryItem_Id], [Tags_Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Customer_Id] in table 'InventoryItems'
ALTER TABLE [dbo].[InventoryItems]
ADD CONSTRAINT [FK_CustomerInventoryItem]
    FOREIGN KEY ([Customer_Id])
    REFERENCES [dbo].[Customers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_CustomerInventoryItem'
CREATE INDEX [IX_FK_CustomerInventoryItem]
ON [dbo].[InventoryItems]
    ([Customer_Id]);
GO

-- Creating foreign key on [InventoryItem_Id] in table 'InventoryItemTag'
ALTER TABLE [dbo].[InventoryItemTag]
ADD CONSTRAINT [FK_InventoryItemTag_InventoryItem]
    FOREIGN KEY ([InventoryItem_Id])
    REFERENCES [dbo].[InventoryItems]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Tags_Id] in table 'InventoryItemTag'
ALTER TABLE [dbo].[InventoryItemTag]
ADD CONSTRAINT [FK_InventoryItemTag_Tag]
    FOREIGN KEY ([Tags_Id])
    REFERENCES [dbo].[Tags]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_InventoryItemTag_Tag'
CREATE INDEX [IX_FK_InventoryItemTag_Tag]
ON [dbo].[InventoryItemTag]
    ([Tags_Id]);
GO

-- Creating foreign key on [Category_Id] in table 'InventoryItems'
ALTER TABLE [dbo].[InventoryItems]
ADD CONSTRAINT [FK_InventoryItemCategory]
    FOREIGN KEY ([Category_Id])
    REFERENCES [dbo].[Categories]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_InventoryItemCategory'
CREATE INDEX [IX_FK_InventoryItemCategory]
ON [dbo].[InventoryItems]
    ([Category_Id]);
GO

-- Creating foreign key on [Category_Id] in table 'Attributes'
ALTER TABLE [dbo].[Attributes]
ADD CONSTRAINT [FK_CategoryAttribute]
    FOREIGN KEY ([Category_Id])
    REFERENCES [dbo].[Categories]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_CategoryAttribute'
CREATE INDEX [IX_FK_CategoryAttribute]
ON [dbo].[Attributes]
    ([Category_Id]);
GO

-- Creating foreign key on [Attribute_Id] in table 'AttributeValues'
ALTER TABLE [dbo].[AttributeValues]
ADD CONSTRAINT [FK_AttributeAttributeValue]
    FOREIGN KEY ([Attribute_Id])
    REFERENCES [dbo].[Attributes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_AttributeAttributeValue'
CREATE INDEX [IX_FK_AttributeAttributeValue]
ON [dbo].[AttributeValues]
    ([Attribute_Id]);
GO

-- Creating foreign key on [InventoryItem_Id] in table 'AttributeValues'
ALTER TABLE [dbo].[AttributeValues]
ADD CONSTRAINT [FK_InventoryItemAttributeValue]
    FOREIGN KEY ([InventoryItem_Id])
    REFERENCES [dbo].[InventoryItems]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_InventoryItemAttributeValue'
CREATE INDEX [IX_FK_InventoryItemAttributeValue]
ON [dbo].[AttributeValues]
    ([InventoryItem_Id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------