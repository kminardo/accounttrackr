#About

Account Trackr is an advanced customer account tracking program. It has the following features:
##This is a wishlist and is currently under development!
##Items may be built in the development branch, they will not be reflected here until moved into master

###Language
- Database: ~~MongoDB~~ Microsoft SQL
- Web Server: C#, WebAPI, MVC5
- Client: HTML, Javascript, AngularJS, jQuery

###Requirements
- Database: ~~MongoDB v2.4.9 or later~~ Microsoft SQL Server Express 2012 or later
- Web Server: IIS 7 or later
- Server Framework: .NET 4.5
- Client Framework/Libraries: AngularJS v1.2.8, Bootstrap v3.0.3, Toastr v2.0.1, jQuery v2.0.3 (All libraries should be pulled automatically from NuGet)
- Developer fueled by Great Lakes Beer

###Full Customer Address Book
- [x] Quickly Add/Remove/Edit Customers
- [ ] Easily control who can manage customer database and how
- [ ] Track customer store credit/gift card
- [ ] Review what rentals the customer has
- [ ] Automatically add fees to customer accounts on a scheduled basis
- [ ] Send emails to late renters

###Full inventory system
- [x] Define your items with an unlimited number of customizable fields (Categories, Genres, etc.)
- [ ] Upload your photos or obtain photos of products from cloud based meta-sources
- [ ] Search for any item in the system by name, attribute or keyword (Keyword/Tag search is currently not a priority- to come later)

###Full POS system
- [ ] Rent your goods with us and keep track of sales with unique identification numbers for products
- [ ] Run reports
	- [ ] What customers are late
	- [ ] What customers are coming up
	- [ ] Total money made
	- [ ] Total percent in fees
	- [ ] Break down of top selling items
- [ ] Track employee sales
- [ ] iPad compatible interface to use as cheap terminals/registers

###Total System control
- [ ] User account based security - Use per site or with employee accounts
- [ ] Track productivity and view sale breakdowns if using employee accounts
- [ ] Customizable menu structure - You decide how display and market your goods

###Full API
- [ ] Provides hooks to obtain and share useful information on a public facing website or mobile app.
- [ ] Show if a product is in stock or when it's due to come back to customers.
- [ ] Give customers public accounts to check due dates, fees and more.

Built with 3 industries in mind: A Library, A Video Store and a Construction Equipment Rental Company for use with Books, Music, Movies or anything else rentable!

###Un-Prioritized Ideas
- [X] Separate Inventory Items from Inventory Categories?
- [ ] Shelf/location tagging?
- [ ] Tagging/Keyword searching?
	- [ ] Johnny Bs Kosher White Flour (Johnny B, Kosher, Flour)
	- [ ] Rons Regular Flour (Rons, Flour) etc.
- [ ] Flexible discount/coupon/rebate functionality

###Technical wishlist
- [ ] Search for customers before adding for duplicates or close names
- [ ] Add some sort of "First Run" configuration to setup user options and database information
- [ ] Edit Category Attributes, Edit item information (Stock counts, category, etc.)